<?php 

/*

Template Name: Nyheter

*/

get_header(); ?>

			<div class="news news-container">
				<h2 class="align-center">Nyheter</h2>
				<div class="row  row-main">

					<?php $posts = get_posts(); ?>
					<?php if( $posts ) : ?>
					<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
					<div class="col-md-4 post">
					<a href="<?php echo get_permalink($post->ID); ?>" >

						<img src="<?php echo get_post_meta($post->ID, 'postthumbnail', true); ?>" />
						<h4><?php echo $post->post_title; ?></h4>
						<p class="date">Skrivet den <span><?php the_date(); ?></span></p>
						<?php the_excerpt(); ?>

					</a>

					</div>
					<?php endforeach; ?>
					<?php endif; ?>

				</div>
			</div>

<?php get_footer(); ?>