<?php 

/*

Template Name: Tjänster

*/

get_header(); ?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images-new/header-img13.jpg'); background-size: cover; background-position: center;">
				<div class="table">
					<div class="table-cell">
						<div class="banner-title-wrapper centered">
							<div class="border header-border"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
							<div class="border header-border"></div>
							<h1 class="page-title">Våra tjänster</h1>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; endif; ?>

			<div class="container-fluid no-padding">
				<div class="row row-main welcome">

					<div class="col-md-9 main-column">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="content">
								<?php the_content(); ?>
								<a title="Om oss" href="<?php the_permalink(24); ?>" class="main-button" id="about-us-button">Läs mer om oss</a>
							</div>
						<?php endwhile; endif; ?>
					</div>

				<div class="col-md-3 contact-column bg-blue">

					<?php include('bookmeeting.php'); ?>
					
				</div>
			</div><!-- /row welcome -->
		</div><!-- /container fluid -->

		<div class="container-fluid no-padding bg-pale-blue">
			<div class="row row-main services">
			<div class="col-12">
				<h2 class="align-center">Våra tjänster</h2>
			</div>
					<?php
					$args = array(
						'post_type' => 'service-startpage'
					); 
					$the_query = new WP_Query( $args );
					

					if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<div class="col-md-4 columns">
						<div class="inner">
							<div class="image-border" style="background-image: url('<?php the_field('bild'); ?>')"></div>
							<div class="text-container">
								<?php the_field('beskrivning'); ?>
							</div>
						</div>
					</div><!--  -->

					<?php endwhile; endif; ?>

			</div><!-- /row services -->
		</div> <!--/container fluid -->

<?php get_footer(); ?>