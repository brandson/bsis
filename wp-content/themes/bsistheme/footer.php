<footer>
	<div class="container-fluid no-padding">
		<div class="row container-footer">
			<div class="col-xl-4 footer-column">
				<p class="c-white"><strong>Vill du att vi kontaktar dig?</strong><br/></p>
					<?php echo do_shortcode('[contact-form-7 id="632" title="Email-form"]');?>
			</div>

			<div class="col-xl-4 footer-column container-logo">
				<div class="wrapper-logo flex-center"><img src="<?php get_the_permalink(); ?>/wp-content/uploads/2017/06/SCAB_ISO_9001_Sve.png" alt="Iso 9001 certifiering" class="footer-icon-round"></div>
				<div class="wrapper-logo flex-center"><img src="<?php get_the_permalink(); ?>/wp-content/uploads/2017/06/SCAB_ISO_14001_Sve.png" alt="Iso 14001 certifiering" class="footer-icon-round"></div>
				<div class="wrapper-logo flex-center"><img src="<?php get_the_permalink(); ?>/wp-content/uploads/2018/10/bisnode-logo.png" alt="Bisnode certifiering" class="footer-icon"></div>
			</div>

			<?php
				$args = array(
					'post_type' => 'contact-info'
				); 
				$the_query = new WP_Query( $args );
			?>

			<div class="col-xl-4 footer-column">
				<div class="row">

					<div class="col-sm-6 footer-sub-column">
						<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<ul class="footer-list">
								<li class="footer-list-item"><a class="footer-link" href="tel:<?php the_field('telefon');?>"><?php the_field('telefon');?></a></p></li> 
								<li class="footer-list-item"><a class="footer-link" href="mailto:<?php the_field('epost');?>"><?php the_field('epost');?></a></p></li>
								<li class="footer-list-item uppercase"><a class="footer-link" href="<?php the_permalink( 20 ); ?>">Kontakta oss1</a></li>
							</ul>
						<?php endwhile; endif; ?> 
					</div>

					<div class="col-sm-6 footer-sub-column">
						<ul class="footer-list">
							<li class="footer-list-item"><a id="to-top" href="#top">Tillbaka till toppen</a></li>
							<li class="footer-list-item uppercase"><a class="footer-link" href="<?php the_permalink( 24 ); ?>">Om BSIS</a></li>
							<li class="footer-list-item uppercase"><a class="footer-link" href="<?php the_permalink( 406 ); ?>">Sitemap</a></li>
						</ul>
					</div>
				</div><!-- /row -->
			</div>

		</div><!-- /row -->
		<div class="row">
			<div class="col bg-black copyright">
				<p class="c-white"><span class="text-big">©</span> BSIS 2018</p>
				<a class="c-white" href="https://www.facebook.com/BSIS-AB-293496074013206/" id="facebook-link"><i class="fab fa-facebook-f"></i></a>
			</div>
		</div>
	</div><!-- /container fluid -->
</footer>
		
		<?php wp_footer(); ?>
<script>
$('wpcf7-submit').on("click", function() {
  window.location.reload(true);
});
</script>
	<script type="text/javascript">
!function(){var t=document.createElement("script"),e="https:"==document.location.protocol?"https://":"http://";t.type="text/javascript",t.async=!0,t.src=e+"s.brandson.se/s.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(t,s)}();

// Lägg till tjänster -->
var BrandsonProspectID = '27406bcec4dbbbbdca8a58c8bb94e7761cf3001b';
var BrandsonTalkID = '50567974f27f1511592fc5378ca3b91f1c79563e';
</script>
<!-- Begin Lead Connect code {/literal} -->
<script>
    var _emv = _emv || [];
    _emv['campaign'] = 'fab6cdec3712c2cb53c399f2';
    _emv['lang'] = 'sv';
    (function() {
        var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true;
        em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'leadconnect.ipmaxi.se/js/leadback.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End Lead Connect code {/literal} -->


	</body>

</html>