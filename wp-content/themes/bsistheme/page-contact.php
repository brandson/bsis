<?php 

/*

Template Name: Kontakt

*/

get_header(); ?>


			<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images/heros/untitled_0200.jpg'); background-size: cover; background-position: center;">
				<div class="table">
					<div class="table-cell">
						<div class="banner-title-wrapper centered">
							<div class="border header-border"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
							<div class="border header-border"></div>
							<h1 class="page-title">Kontakta oss</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid no-padding">
				<div class="row row-main">
					<div class="col-md-8 col-xl-9 container-coworkers">
						<div class="row">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<div class="col-12">
								<h2 class="centered">Våra medarbetare</h2>
							</div> 
						<?php if( have_rows('coworkers-repeater') ):
							while ( have_rows('coworkers-repeater') ) : the_row(); ?>
						
							<div class="col-sm-6 col-xl-4">
							<div class="card">
								<img src="<?php the_sub_field('img'); ?>" class="profile-img" alt="Profilbild">
								<div class="profile-info-wrapper">
									<h4 class="centered"><?php the_sub_field('name'); ?></h4>
									<p class="centered"><?php the_sub_field('title'); ?></p>
								</div>
								<hr>
								<div class="profile-info-wrapper">
									<a href="tel:<?php the_sub_field('phone'); ?>"><?php the_sub_field('phone'); ?></a><br/>
									<a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
								</div>
							</div>
						</div>
						
						<?php endwhile;
								else :
							echo 'no rows found';
								endif;
									endwhile; endif;
						?>
						</div>		
						</div><!-- /row --> 

					<div class="col-md-4 col-xl-3">
							<div class="col-12 bg-blue contact-column">
								<?php include('bookmeeting.php'); ?>
							</div>
					<?php
						$args = array(
							'post_type' => 'contact-info'
						); 
						$the_query = new WP_Query( $args );
					?>
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="col-12 bg-pale-blue">
							<div class="contact-content">
							<h3 class="centered">Kontaktuppgifter</h3>
								<ul class="contact-info">
									<li><label>Telefon:</label> <br><a href="tel:<?php the_field('telefon'); ?>"><?php the_field('telefon'); ?></a></li>
									<li><label>Email:</label> <br><a href="mailto:<?php the_field('epost'); ?>"><?php the_field('epost'); ?></a></li>
									<li><label>Adress:</label><?php the_field('adress'); ?></li>
								</ul>
							</div>
						</div>
					<?php endwhile; endif; ?>
					</div>
				</div><!-- /row -->
			</div><!-- /container-fuid -->

<!--
	[contact-form-7 id="123" title="contactform"]
-->

<?php get_footer(); ?>