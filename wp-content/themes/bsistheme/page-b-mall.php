<?php 

/*

Template Name: B-Mall

*/
global $post;
$post_title = $post->post_title;

get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images/heros/untitled_0303.jpg'); background-size: cover; background-position: center;">
	<div class="table">
		<div class="table-cell">
			<div class="banner-title-wrapper centered">
				<div class="border header-border"></div>
				<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
				<div class="border header-border"></div>
				<h1 class="page-title">Om oss</h1>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid no-padding">
	<div class="row row-main">
		<div class="col-md-9 main-column page-b">
			<div class="content">
				<?php the_content(); ?>
			</div><!-- /content -->
		</div><!-- /main column -->

		<div class="col-md-3 bg-blue contact-column">
			<?php include('bookmeeting.php'); ?>
		</div>
	</div><!-- /row -->
	<?php endwhile; endif; ?>
	<?php
					$args = array(
						'post_type' => 'contact-info'
					); 
					$the_query = new WP_Query( $args );
				?>
	<?php

				$args = array(

					'post_type' => 'punchline-area',
					'post-taxonomy' => 'start-punchline'

				); 

				$the_query = new WP_Query( $args );

			?>

	<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

	<?php endwhile; endif; ?>

	<div class="row row-main">
		<div class="col-lg-12 columns partners">
			<h2 class="align-center">Vi är medlemmar i</h2>

			<?php

						$args = array(

							'post_type' => 'partners'

						); 

						$the_query = new WP_Query( $args );

					?>

			<div class="partner-container">
				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="client-wrapper">
					<img src="<?php the_field('bild'); ?>" alt="<?php the_field('bild_alt'); ?>" class="partner-logo" />
				</div>

				<?php endwhile; endif; ?>
			</div><!-- /partner-container -->

		</div>
	</div><!-- /row partners -->

	<?php get_footer(); ?>