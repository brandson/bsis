jQuery(window).load(function() {
   jQuery('.flexslider').flexslider();
});

jQuery(window).on("resize", function () {
    var height = jQuery('.flexslider').height();
    var width = jQuery('.flexslider').width();
    jQuery('.flexslider .slides li').css('height', height).css('width',width);
}).resize();

jQuery(function() {
  jQuery('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        jQuery('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});

jQuery('.form-container').hide();

jQuery(document).ready(function(){
	jQuery('#show-form').click(function(e){
		e.preventDefault(); 
		jQuery('.form-container').show();		
	});
});

jQuery(document).ready(function(){
	jQuery('#close-form').click(function(e){
		e.preventDefault(); 
		jQuery('.form-container').hide();		
	});
});

jQuery(document).ready(function(){
    jQuery('#show-menu').click(function(e){
        e.preventDefault(); 
        jQuery('.mobile-menu ul').slideToggle(300);       
    });
});

jQuery(document).ready(function(){
  if(!!('ontouchstart' in window)){
    jQuery('.fast-facts').on('click', function(){
      jQuery('.fast-facts').toggleClass('fast-facts-show');   
    });
  }
  else{
    jQuery('.fast-facts').hover(function(){
        jQuery('.fast-facts').toggleClass('fast-facts-show');       
    });
  }
});


