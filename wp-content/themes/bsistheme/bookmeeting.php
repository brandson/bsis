<?php 

/*

Template Name: Bookmeeting

*/

?>

<div class="icon-container">
	<a href="" id="show-form" class="icon-link">
		<div class="icon-border">
			<i class="far fa-envelope icon-big"></i>
		</div>
		<p class="book-meeting-link">Kontakta oss</p>
	</a>
</div>