<?php 

/*

Template Name: byggservice

*/

get_header(); ?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


			<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images-new/header-img21.jpg'); background-size: cover; background-position: top center;">
				<div class="table">
					<div class="table-cell">
						<div class="banner-title-wrapper centered">
							<div class="border header-border"></div>
								<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
							<div class="border header-border"></div>
							<h1 class="page-title">Byggservice</h1>
						</div>
					</div>
				</div>
			</div>

		<div class="container-fluid no-padding">
			<div class="row row-main">
				<div class="col-md-9 main-column page-b">
					<div class="content">
						<?php the_content(); ?>
					</div><!-- /content -->	
				</div><!-- /main column -->

				<div class="col-md-3 main-column bg-blue contact-column">
					<?php include('bookmeeting.php'); ?>
				</div>
			</div><!-- /row -->	
		</div><!-- /container-fluid -->
	<?php endwhile; endif; ?>

		<div class="container-fluid no-padding bg-pale-blue">
			<div class="row row-main services services2">
				<div class="col-12">
					<h2 class="align-center">Sammanfattning av våra Byggtjänster</h2>
				</div>

					<?php
						$args = array(
							'post_type' => 'constructionservices'
						); 
						$the_query = new WP_Query( $args );
					?>

					<div class="col-md-6 service-list">
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<h4 class="under-service"><?php the_field('namn'); ?></h4>
					<?php endwhile; endif; ?>
					</div>

				<div class="col-md-6 service-links">
					<div class="service-link-wrapper">
						<a href="http://www.bsis.se/foretagstjanster/">
							<i class="fas fa-angle-right"></i>
							Läs mer om våra företagstjänster
						</a>
						<a href="http://www.bsis.se/foretagstjanster/">
							<i class="fas fa-angle-right"></i>
							Läs mer om våra underhållstjänster
						</a>
					</div>
				</div>
			</div><!-- /row -->
		</div><!-- /container -->


<?php get_footer(); ?>