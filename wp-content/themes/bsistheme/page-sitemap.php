<?php 

/*

Template Name: Sitemap

*/

get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images-new/header-img4.jpg'); background-size: cover; background-position: center;">
	<div class="table">
		<div class="table-cell">
			<h1 class="page-title">Sitemap</h1>
		</div>
	</div>
</div>

<div class="container-fluid no-padding">
			<div class="row">
				<div class="col main-column sitemap-wrapper">
					<div class="content">
						<ul class="sitemap-list">
							<?php wp_list_pages('title_li'); ?>
						</ul>
					</div><!-- /content -->	
				</div><!-- /main column -->	

			</div><!-- /row  -->	
</div><!-- /container fluid -->
<?php endwhile; endif; ?>

<div class="container-fluid no-padding">
				<div class="row  row-main clients">
					<div class="col-lg-12 columns">
						<h2 class="align-center">Ett urval av våra kunder</h2>

						<?php

							$args = array(

								'post_type' => 'clients'

							); 

							$the_query = new WP_Query( $args );

						?>
						
						<div class="client-container">
							<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="client-wrapper">
									<img src="<?php the_field('kundlogo'); ?>" alt="<?php the_field('bild_alt'); ?>" class="client-logo"/>
								</div>
							<?php endwhile; endif; ?>
						</div>

					</div>
				</div><!-- /row clients -->
			</div><!-- /container fluid -->

			<?php

				$args = array(

					'post_type' => 'punchline-area',
					'post-taxonomy' => 'start-punchline'

				); 

				$the_query = new WP_Query( $args );

			?>

			<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="punchline" style="background: url(<?php the_field('bakgrundsbild'); ?>); background-size: cover; background-position: center;">
				<div class="row  row-main">
					<div class="col-lg-12 columns">

						<h2 class="align-center"><?php the_field('punchline'); ?></h2>

					</div>
				</div>
			</div>

			<?php endwhile; endif; ?>

			<div class="row row-main">
				<div class="col-lg-12 columns partners">
					<h2 class="align-center">Vi är medlemmar i</h2>

					<?php

						$args = array(

							'post_type' => 'partners'

						); 

						$the_query = new WP_Query( $args );

					?>

					<div class="partner-container">
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
					
						<div class="client-wrapper">
							<img src="<?php the_field('bild'); ?>" alt="<?php the_field('bild_alt'); ?>" class="partner-logo"/>
						</div>

					<?php endwhile; endif; ?>
					</div><!-- /partner-container -->

				</div>
			</div><!-- /row partners -->		

			<div class="news" style="display: none;">
				<h2 class="align-center">Senaste nyheter</h2>
				<div class="row  row-main">

					<?php $posts = get_posts( "numberposts=3" ); ?>
					<?php if( $posts ) : ?>
					<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
					<div class="medium-4 columns end post">
					<a href="<?php echo get_permalink($post->ID); ?>" >

						<img src="<?php echo get_post_meta($post->ID, 'postthumbnail', true); ?>" />
						<h4><?php echo $post->post_title; ?></h4>
						<p class="date">Skrivet den <span><?php the_date(); ?></span></p>
						<?php the_excerpt(); ?>

					</a>

					</div>
					<?php endforeach; ?>
					<?php endif; ?>

				</div>
				<a href="http://labs.digcy.se/bsis/?page_id=255" class="show-all">Visa alla nyheter</a>
			</div>

<?php get_footer(); ?>