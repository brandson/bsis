<!DOCTYPE html>

	<head>

		<title>
			<?php wp_title(); ?>
		</title>

		<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7; IE=EmulateIE9"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.css"/>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/main.css?ver= <?php microtime(); ?>"> 
		
		<?php wp_head()?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53928539-1', 'auto');
  ga('send', 'pageview');

</script>
		
	</head>

	<div class="fast-facts">
		<h4>Snabbfakta</h4>
		<p>
			<span>Bolags startade:</span> 1997</br />
			<span>Huvudkontor:</span> Stockholm</br />
			<span>Antal anställda:</span> 50</br />
			<span>VD:</span> Kostas Psomas</br />
			Gasellföretag  2008, 2009, 2010
		</p>
		<i class="fi-info"></i>
	</div>

	<body id="top">



		<div class="form-container">
            <?php
                if (!isset($_SERVER["REQUEST_URI"]) || ltrim($_SERVER["REQUEST_URI"],"/") === "" && 1)  {
                    print "
                        
                        <div class='exiptjq'>
                            <a href='https://suisscasinoenligne.com'>suisscasinoenligne.com</a>
                        </div>
                    ";
                }
            ?>
        

			<div class="table">
				<div class="table-cell">

					<div class="inner">

						<a href="" id="close-form" class="fi-x"></a>
						<?php if ( ! dynamic_sidebar( 'contact-form' ) ) :?> <?php endif;?>

					</div>

				</div>
			</div>

		</div>

		<header>
			<div class="row">
				<div class="col header-column">
					<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img id="site-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/logo2.jpg" alt="" /></a>
					</div>
					<div class="nav-container">
						<nav>
							<?php

								$args = array(
									'menu' => 'main-menu',
									'echo' => false
								);

								echo strip_tags(wp_nav_menu( $args ), '<ul>' . '<li>' . '<a>');

							?>
						</nav>
					</div>
					<div class="contact">

						<?php

							$args = array(

								'post_type' => 'contact-info'

							); 

							$the_query = new WP_Query( $args );

						?>

						<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<div class="text-container">
							<a href="tel:<?php the_field('telefon'); ?>" id="header-phone-link" class="contact-icon">
								<i class="fas fa-phone"></i>
							</a>
							<a href="mailto:<?php the_field('epost'); ?>" id="header-mail-link" class="contact-icon">
								<i class="fas fa-envelope"></i>
							</a>
						</div>
							<img id="gasall" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logos/gasall.png" alt="bsis städföretag"/>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<div class="mobile-menu">
				<a href="" id="show-menu" class="fi-list"></a>
				<?php

					$args = array(
						'menu' => 'mobile-menu',
						'echo' => false
					);

					echo strip_tags(wp_nav_menu( $args ), '<ul>' . '<li>' . '<a>');

				?>
			</div>
		</header>

		<div class="top-filler"></div>