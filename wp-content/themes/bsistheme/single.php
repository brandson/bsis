<?php get_header(); ?>

					<div class="row single-post">

						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<div class="col-lg-8 centered main-column">

							<h3><?php the_title(); ?></h3>
							<p class="date">Skrivet den <span><?php the_date(); ?></span></p>
							<?php the_content(); ?>

						</div>

						<?php endwhile; endif; ?>

					</div>

					<div class="news-links">
						<?php previous_post('%',
 'Läs förgående inlägg', 'no'); ?>
						<?php next_post('%',
 'Läs nästa inlägg', 'no'); ?>
					</div>

<?php get_footer(); ?>