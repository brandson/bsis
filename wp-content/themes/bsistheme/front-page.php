<?php 

/*

Template Name: Start

*/

get_header(); ?>

			<div class="flexslider">
				<div class="slides">

					<?php
						$args = array(
							'post_type' => 'slider'
						); 
						$the_query = new WP_Query( $args );
					?>

					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li id="header-slider" style="background: url(<?php the_field('bakgrundsbild'); ?>); background-size: cover; background-position: center;">
						<div class="table">
							<hgroup class="table-cell main-column">
								<div class="slider-title-wrapper centered">
									<div class="border header-border"></div>
										<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
									<div class="border header-border"></div>
									<h1 class="page-title"><?php the_field('textbig'); ?></h1>
									<a href="<?php the_permalink( 22 ); ?>" class="main-button">
										Våra tjänster
									</a>
								</div>
							</hgroup>
						</div>
					</li><!-- /Header slider -->
					<?php endwhile; endif; ?>
				</div>
			</div><!-- /flexslider -->

		<div class="container-fluid no-padding">
			<div class="row row-main welcome">

				<div class="col-md-9 main-column">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="content">
							<?php the_content(); ?>
							<a title="Om oss" href="<?php the_permalink(24); ?>" class="main-button" id="about-us-button">Läs mer om oss</a>
						</div>
					<?php endwhile; endif; ?>
				</div>

				<div class="col-md-3 bg-blue contact-column">
					<?php include('bookmeeting.php'); ?>
				</div>

			</div><!-- /row welcome -->
		</div><!-- /container fluid -->

		<div class="container-fluid no-padding bg-pale-blue">
			<div class="row row-main services">
				<div class="col-12">
					<h2 class="align-center">Våra tjänster</h2>
				</div>
					<?php
					$args = array(
						'post_type' => 'service-startpage'
					); 
					$the_query = new WP_Query( $args );
					

					if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

					<div class="col-md-4 columns">
						<div class="inner">
							<div class="image-border" style="background-image: url('<?php the_field('bild'); ?>')"></div>
							<div class="text-container">
								<?php the_field('beskrivning'); ?>
							</div>
						</div>
					</div>

					<?php endwhile; endif; ?>

			</div><!-- /row services -->
		</div> <!--/container fluid -->
						
			<div class="container-fluid no-padding">
				<div class="row row-main clients">
					<div class="col-lg-12 columns">
						<h2 class="align-center">Ett urval av våra kunder</h2>

						<?php

							$args = array(

								'post_type' => 'clients'

							); 

							$the_query = new WP_Query( $args );

						?>
						
						<div class="client-container">
							<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="client-wrapper">
									<img src="<?php the_field('kundlogo'); ?>" alt="<?php the_field('bild_alt'); ?>" class="client-logo"/>
								</div>
							<?php endwhile; endif; ?>
						</div>

					</div>
				</div><!-- /row clients -->
			</div><!-- /container fluid -->

			<?php

				$args = array(

					'post_type' => 'punchline-area',
					'post-taxonomy' => 'start-punchline'

				); 

				$the_query = new WP_Query( $args );

			?>

			<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="punchline" style="background: url(<?php the_field('bakgrundsbild'); ?>); background-size: cover; background-position: center;">
				<div class="row row-main">
					<div class="col-lg-12 columns">

						<h2 class="align-center"><?php the_field('punchline'); ?></h2>

					</div>
				</div>
			</div>

			<?php endwhile; endif; ?>

			<div class="row row-main">
				<div class="col-lg-12 columns partners">
					<h2 class="align-center">Vi är medlemmar i</h2>

					<?php

						$args = array(

							'post_type' => 'partners'

						); 

						$the_query = new WP_Query( $args );

					?>

					<div class="partner-container">
					<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>	
					
						<div class="client-wrapper">
							<img src="<?php the_field('bild'); ?>" alt="<?php the_field('bild_alt'); ?>" class="partner-logo"/>
						</div>

					<?php endwhile; endif; ?>
					</div><!-- /partner-container -->

				</div>
			</div><!-- /row partners -->		

			<div class="news" style="display: none;">
				<h2 class="align-center">Senaste nyheter</h2>
				<div class="row row-main">

					<?php $posts = get_posts( "numberposts=3" ); ?>
					<?php if( $posts ) : ?>
					<?php foreach( $posts as $post ) : setup_postdata( $post ); ?>
					<div class="col-md-4 columns end post">
					<a href="<?php echo get_permalink($post->ID); ?>" >

						<img src="<?php echo get_post_meta($post->ID, 'postthumbnail', true); ?>" />
						<h4><?php echo $post->post_title; ?></h4>
						<p class="date">Skrivet den <span><?php the_date(); ?></span></p>
						<?php the_excerpt(); ?>

					</a>

					</div>
					<?php endforeach; ?>
					<?php endif; ?>

				</div>
				<a href="http://labs.digcy.se/bsis/?page_id=255" class="show-all">Visa alla nyheter</a>
			</div>
		</div> <!--/ Container fluid -->
<?php get_footer(); ?>