<?php

	// enable custom menus
	add_theme_support( 'menus' );

	add_filter( 'show_admin_bar', '__return_false' );

	function any_ptype_on_cat($request) {
	if ( isset($request['category_name']) )
		$request['post_type'] = 'any';

	return $request;
	}
	add_filter('request', 'any_ptype_on_cat');
	
	// Load theme CSS
	function theme_styles() {
		wp_enqueue_style( 'webfonts', get_template_directory_uri() . '/stylesheets/webfonts.css' );
		wp_enqueue_style( 'foundation', get_template_directory_uri() . '/stylesheets/foundation.css' );
		wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/stylesheets/flexslider.css' );
		wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.css' );
		wp_enqueue_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Roboto:400,700' );
		//wp_enqueue_style( 'googlefonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' );
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css');
		wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css' );
	}

	// Load theme JS
	function theme_js() {

		wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '', true );
		wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/foundation.js', array(), '', true );
		wp_enqueue_script( 'themejs', get_template_directory_uri() . '/js/theme.js', array('jquery'), '', true );

	}

	add_action( 'wp_enqueue_scripts', 'theme_js' );

	add_action( 'wp_enqueue_scripts' , 'theme_styles' );

	function arphabet_widgets_init() {

		register_sidebar( array(
			'name' => 'contact-form',
		) );

	}

	add_action( 'widgets_init', 'arphabet_widgets_init' );

	add_theme_support('post-thumbnails',array('post'));

	function custom_excerpt_length( $length ) {
        return 20;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
    add_filter( 'wpcf7_validate_configuration', '__return_false' );
	
	add_filter(‘acf/settings/remove_wp_meta_box’, ‘__return_false’);

?>