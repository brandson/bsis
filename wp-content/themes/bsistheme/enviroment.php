<?php 

/*

Template Name: Kvalitet

*/

get_header(); ?>

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="page-banner" style="background: url('<?php echo get_template_directory_uri(); ?>/images-new/header-img20.jpg'); background-size: cover; background-position: top center;">
				<div class="table">
					<div class="table-cell">
						<div class="banner-title-wrapper centered">
							<div class="border header-border"></div>
							<img src="<?php echo get_template_directory_uri(); ?>/images/logos/cleaning-logo.png" id="slider-icon">
							<div class="border header-border"></div>
							<h1 class="page-title">Kvalitet & Miljö</h1>
						</div>
					</div>
				</div>
			</div>

		<div class="container-fluid no-padding">
			<div class="row row-main">
				<div class="col-md-9 main-column page-b">
					<div class="content">
						<?php the_content(); ?>
					</div><!-- /content -->	
				</div><!-- /main column -->

				<div class="col-md-3 bg-blue contact-column">
					<?php include('bookmeeting.php'); ?>
				</div>
			</div><!-- /row welcome -->	
	<?php endwhile; endif; ?>

			<div class="page-content lightblue">

				<?php

					$args = array(

						'post_type' => 'quality',
						'post-taxonomy' => 'kvalitet'

					); 

					$the_query = new WP_Query( $args );

				?>

				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="container">
					<div class="row row-main">
						<div class="col-lg-8 margin-auto">

							<?php the_field('text'); ?>

						</div>
					</div>
				</div>

				<?php endwhile; endif; ?>

			</div>

			<?php

				$args = array(

					'post_type' => 'punchline-area',
					'post-taxonomy' => 'enviroment-punchline'

				); 

				$the_query = new WP_Query( $args );

			?>

			<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="punchline" style="background: url(<?php the_field('bakgrundsbild'); ?>); background-size: cover; background-position: top center;">
				<div class="row row-main">
					<div class="col-md-8 centered">

						<h2 class="centered"><?php the_field('punchline'); ?></h2>

					</div>
				</div>
			</div>

			<?php endwhile; endif; ?>

			<div class="page-content white">

				<?php

					$args = array(

						'post_type' => 'quality',
						'post-taxonomy' => 'miljo'

					); 

					$the_query = new WP_Query( $args );

				?>

				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="container">
					<div class="row row-main">
						<div class="col-md-8 centered">

							<?php the_field('text'); ?>

						</div>
					</div>
				</div>

				<?php endwhile; endif; ?>

			</div>

			<div class="page-content lightblue">

				<?php

					$args = array(

						'post_type' => 'quality',
						'post-taxonomy' => 'workenviroment'

					); 

					$the_query = new WP_Query( $args );

				?>

				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="container">
					<div class="row row-main">
						<div class="col-md-8 centered">

							<?php the_field('text'); ?>

						</div>
					</div>
				</div>

				<?php endwhile; endif; ?>

			</div>

<?php get_footer(); ?>